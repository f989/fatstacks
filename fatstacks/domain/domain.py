from fatstacks.domain.typings import Enumeration, Generic, T


class Asset:
	...


class Symbol:
	...


class Portfolio:
	...


class Action( Enumeration ):
	SELL = 1
	BUY = 2


class Signal:
	...


class Alert:
	...


class Strategy:
	...


class Event( Generic[ T ] ):
	...
