import json
import os

import psycopg2
from kafka import KafkaProducer

from fatstacks.implementations.binanceClient import ApiAuthorization, BinanceChannel

# region acquisition
KAFKA_HOST = os.environ.get( "kafka-host", "localhost:29092" )
CONNECTION = os.environ.get( "db-connection", "postgres://postgres:password@localhost:5432" )
credentials = ApiAuthorization(
	api_key=os.environ[ "api-key" ],
	api_secret=os.environ[ "api-secret" ]
)
# endregion

# region initialization
asset_id = 1
symbol = "BTCUSDT"
create_sql = """
create schema if not exists historical;

-- Base Definitions
create table if not exists historical.assets
(
	asset_id serial,
	symbol   varchar(32) not null,
	name     varchar(32) not null,
	fiat     varchar(32) not null,
	metadata json        null,
	primary key ( asset_id)


);
create table if not exists historical.candles
(

	time                   timestamp not null,
	asset_id               integer   not null,
	open                   float     not null,
	close                  float     not null,
	high                   float     not null,
	low                    float     not null,

	base_volume            float     not null,
	taker_buy_base_volume  float     not null,
	quote_volume           float     not null,
	taker_buy_quote_volume float     not null,
	count                  integer   not null
);

-- Timescale Hypertable
select
public.create_hypertable( 'historical.candles',
 	                          'time', 'asset_id',
 	                          number_partitions := 10, if_not_exists := true );
 
-- Timescale Compression
-- alter table historical.candles
-- 	set (
-- 		timescaledb.compress,
-- 		timescaledb.compress_segmentby = 'asset_id',
-- 		timescaledb.compress_orderby = 'time'
--		);
-- select public.add_compression_policy( 'historical.candles', interval '7 days', if_not_exists := true );
"""
asset_sql = f"""
INSERT INTO 
historical.assets( asset_id , symbol, name, fiat )
values 
( {asset_id} , '{symbol}' , 'Bitcoin' , 'Dollar' )
ON CONFLICT DO NOTHING"""


# endregion

def to_dict(ticker):
	return {
		"time": ticker[ "t" ], "asset_id": asset_id,
		"open": ticker[ "o" ], "close": ticker[ "c" ], "high": ticker[ "h" ], "low": ticker[ "l" ],
		"base_volume": ticker[ "v" ], "quote_volume": ticker[ "q" ],
		"taker_buy_base_volume": ticker[ "V" ], "taker_buy_quote_volume": ticker[ "Q" ],
		"count": ticker[ "n" ]
	}


def handle_ticker(kafka_producer, database_cursor, payload):
	print( payload )

	ticker = payload[ "k" ]
	if ticker[ "x" ]:
		ticker = to_dict( ticker )
		try:
			database_cursor.execute( """
				insert into historical.candles
				( time, asset_id, 
				open, close, high, low, 
				base_volume, quote_volume ,
				taker_buy_base_volume , taker_buy_quote_volume, 
				 count )
				values 
				( to_timestamp( %(time)s ::numeric / 1000), %(asset_id)s,
				%(open)s , %(close)s , %(high)s , %(low)s,
				%(base_volume)s , %(quote_volume)s,
				%(taker_buy_base_volume)s , %(taker_buy_quote_volume)s, 
				%(count)s)
				""", ticker )
			conn.commit()
		except Exception as e:
			print( e )
		try:
			kafka_producer.send( "timescale.historical.candles", ticker )
		except Exception as e:
			print( e )


if __name__ == '__main__':
	print( "Starting Binance Data Collection!" )

	conn = psycopg2.connect( CONNECTION )
	cursor = conn.cursor()
	cursor.execute( create_sql )
	cursor.execute( asset_sql )
	print( "Sucessfuly Connected to Database!" )

	producer = KafkaProducer( bootstrap_servers=KAFKA_HOST,
	                          value_serializer=lambda v: json.dumps( v ).encode( 'utf-8' ) )
	print( "Sucessfuly Connected to Kafka!" )

	channel = BinanceChannel( credentials )
	print( "Sucessfuly Connected to Binance!" )

	callback = lambda x: handle_ticker( producer, cursor, x )
	channel.subscribe( "BTCUSDT", callback )
