from __future__ import annotations

from binance import ThreadedWebsocketManager
from typing import NamedTuple, Callable

from fatstacks.domain.domain import Symbol
from fatstacks.domain.typings import Enumeration, String, Name

class ApiEnvironment( Enumeration ):
	LIVE = 'https://binance.vision/api'
	TEST = 'https://testnet.binance.vision/api'

class ApiAuthorization( NamedTuple ):
	#TODO  2021-10-23 use dataclasses
	api_key: String
	api_secret: String

class BinanceChannel:

	def __init__(self, auth: ApiAuthorization,
	             environment: ApiEnvironment = ApiEnvironment.TEST):
		is_test = (environment == ApiEnvironment.TEST)

		bsm = ThreadedWebsocketManager( auth.api_key, auth.api_secret, testnet=is_test )
		bsm.start()

		self.bsm: ThreadedWebsocketManager = bsm

	def subscribe(self, asset: Name[ Symbol ], action: Callable) -> None:
		self.bsm.start_kline_socket( callback=action, symbol=asset )
