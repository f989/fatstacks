create schema if not exists historical;

-- Base Definitions
create table if not exists historical.assets
(
	asset_id serial,
	symbol   varchar(32) not null,
	name     varchar(32) not null,
	fiat     varchar(32) not null,
	metadata json        null


);
create table if not exists historical.candles
(

	time                   timestamp not null,
	asset_id               integer   not null,
	open                   float     not null,
	close                  float     not null,
	high                   float     not null,
	low                    float     not null,

	base_volume            float     not null,
	taker_buy_base_volume  float     not null,
	quote_volume           float     not null,
	taker_buy_quote_volume float     not null,
	count                  integer   not null
);

-- create unique index if not exists un_asset_time on historical.candles ( asset_id, time desc );

-- Timescale Hypertable
select
	public.create_hypertable( 'historical.candles',
	                          'time', 'asset_id',
	                          number_partitions := 10, if_not_exists := true );

-- Timescale Compression
alter table historical.candles
	set (
		timescaledb.compress,
		timescaledb.compress_segmentby = 'asset_id',
		timescaledb.compress_orderby = 'time'
		);
select public.add_compression_policy( 'historical.candles', interval '7 days', if_not_exists := true );

