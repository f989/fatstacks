FROM python:3.8-slim as build


COPY ./requirements.txt $WORKDIR/
RUN pip install --user -r requirements.txt
COPY ./fatstacks/ $WORKDIR/fatstacks
COPY ./main.py $WORKDIR/


CMD ["python" , "main.py"]
