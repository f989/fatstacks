import datetime

from typing import TypeVar, Protocol, Generic, Dict, Literal, Callable

# noinspection PyStatementEffect
Protocol, Generic, Dict, Literal, Callable

T = TypeVar( "T", covariant=True )
K = TypeVar( "K", covariant=True )
V = TypeVar( "V", covariant=True )
U = TypeVar( "U", covariant=True )

String = str
Temporal = datetime.datetime
Annotated = Generic
Enumeration = object
Stream = Generic
Name = Generic
Delta = Generic
Cost = float
Ratio = float
Volume = float
Count = int
